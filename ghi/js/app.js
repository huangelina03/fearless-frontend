function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card mb-3 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card">
          <div class="card-footer text-muted">${startDate}-${endDate}</div>
          </div>
        </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        const cardhtmls = [];
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts).toLocaleDateString("en-US");
            const endDate = new Date(details.conference.ends).toLocaleDateString("en-US");
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, startDate, endDate, location);
            cardhtmls.push(html)
          }

        }
        console.log(cardhtmls)
        const columns = document.querySelectorAll('.col-md-4');
        console.log(columns)
        columns.forEach((column, index) => {
                column.innerHTML += cardhtmls[index];
            })
      }
    } catch (e) {
        console.error(e);
    }

  });




// const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();
//           const descriptionTag = document.querySelector('.card-text');
//           descriptionTag.innerHTML = details.conference.description;
//           const imageTag = document.querySelector('.card-img-top');
//           imageTag.src = details.conference.location.picture_url;


          // const detailUrl = `http://localhost:8000${conference.href}`;
          // const detailResponse = await fetch(detailUrl);
          // if (detailResponse.ok) {
          //   const details = await detailResponse.json();
          //   const title = details.conference.title;
          //   const description = details.conference.description;
          //   const pictureUrl = details.conference.location.picture_url;
          //   const html = createCard(title, description, pictureUrl);
          //   const column = document.querySelector('.col');
          //   column.innerHTML += html;
